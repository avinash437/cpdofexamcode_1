package com.agiletestingalliance;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.qaagility.controller.About;

public class AboutCPDOFTest {

	@Test
	public void testdesc() {

		String s = new AboutCPDOF().desc();
		assertTrue("page", s.contains("CP-DOF certification"));

	}

}

