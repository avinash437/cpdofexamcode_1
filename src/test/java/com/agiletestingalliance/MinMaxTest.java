package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class MinMaxTest {

	@Test
	public void testfindMax() throws Exception {

		int k = new MinMax().findMax(15, 3);
		assertEquals("findMax", 15, k);

	}

	@Test
	public void testfindMax1() throws Exception {

		int k = new MinMax().findMax(2, 5);
		assertEquals("findMax", 5, k);

	}
}

